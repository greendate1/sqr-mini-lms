// User balance should be different for each user
// Base currency: USD

function isValidCardNum(num) { }
function isValidCardDate(expiryMonth, expiryYear) { }
function isValidCVC(cvc) { }

function getBalance() { }

function topupBalance(amount) { }

function decreaseFromBalance(amount) { }

// The amount will be added to the balance
// 1 USD = 1 Balance point
// Validates card data first
function payWithCard(amount) { }

// Return whether is more than or equal to amount
function canAfford(amount) { }

// Optional: returns a list of purchases
function getPurchases() { }

/* External API Magic */
function amountInRUB(amountInUSD) { }

function amountInEUR(amountInUSD) { }

