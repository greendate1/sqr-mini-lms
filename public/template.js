setTimeout(() => {
  const template = document.querySelector('#dashboard')

  const content = template.content
  const contentToInput = document.querySelector('#root')
  console.log(contentToInput)
  content.querySelector('#content-parent').appendChild(contentToInput)

  const clone = document.importNode(content, true)
  document.body.appendChild(clone)
}, 50)

function replaceTitle(newTitle) {
  setTimeout(() => {
    const titleElement = document.querySelector('#layout-title')
    titleElement.innerHTML = newTitle
  }, 50)
}
