
class Lesson {
  constructor(id, title, body, locked) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.locked = locked;
  }
}

// Locked state should be different for each user
let paid = []

let lessons = [
    new Lesson(0,
     "Adaptive Software Development Lesson",
     `Adaptive Software Development is a move towards adaptive practices,
      leaving the deterministic practices in the context of complex systems and
      complex environments. Adaptive Software Development focuses on collaboration
      and learning as a technique to build complex systems.
      It is evolved from the best practices of Rapid Application Development (RAD)
      and Evolutionary Life Cycles.`,
     true),
    new Lesson(1,
     "Artificial Intelligence Lesson",
     `This tutorial provides introductory knowledge on Artificial Intelligence.
      It would come to a great help if you are about to select Artificial Intelligence
      as a course subject.
      You can briefly know about the areas of AI in which research is prospering.`,
     false),
    new Lesson(2,
     "Inter Process Communication Lesson",
     `Inter Process Communication (IPC) refers to a mechanism, where the
      operating systems allow various processes to communicate with each other.
      This involves synchronizing their actions and managing shared data.
      This tutorial covers a foundational understanding of IPC.
      Each of the chapters contain related topics with simple and useful examples.`,
     false),
    new Lesson(3,
     "Cloud Computing Lesson",
     `Cloud Computing provides us means by which we can access the applications
      as utilities over the internet.
      It allows us to create, configure, and customize the business applications online.`,
     true),
    new Lesson(4,
     "Compiler Design Lesson",
     `A compiler translates the code written in one language to some other
      language without changing the meaning of the program. It is also expected
      that a compiler should make the target code efficient and optimized in
      terms of time and space.`,
     false)];

// Returns lessons from an array (for example)
// If locked, do not include body
function getLessons() {
  let answer = []

  for (let i = 0; i < lessons.length; i++) {
      if (lessons[i].locked == false) answer.push(lessons[i]);
      else answer.push(new Lesson(lessons[i].id, lessons[i].title, "Unavailable lesson", true));
  }

  let disp = '';
  for (let i = 0; i < answer.length; i++) {
      let lesson = answer[i];
      disp += `<div>
      <h5>${lesson.title}</h5>
      <p>${lesson.body}</p>`
      if(lesson.locked) disp += `<button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onclick="unlockLesson(${lesson.id})">Unlock lesson </button>`
      disp += `</div> <hr>`
  }
  gbid('lessons').innerHTML = disp;
}

// Checks for price and balance and saves lesson as unlocked
// Uses payments methods
function unlockLesson(lessonId) {
    if(getBalance() >= 1) {
      lessons[lessonId].locked = false;
      alert(lessonId);
      getLessons();
      addMoneyToBalance(-1);
      alert("New lesson unlocked.")
    }
    else {
      alert("Insufficient credit to unlock this lesson.")
    }
 }
