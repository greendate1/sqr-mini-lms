const gbid = (id) => document.getElementById(id);
const saveValue = (key, value) => localStorage.setItem(key, value);
const loadValue = (key) => localStorage.getItem(key);