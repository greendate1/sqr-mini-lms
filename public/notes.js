// Notes should different for each user

let notes = [];
let editingNoteId = null;

let newTitle, newBody;

function initNotes() {
    setTimeout(() => {
        newTitle = gbid('newTitle');
        newBody = gbid('newBody');
        loadNotes();
        refreshNotes();
        if (isLimited()) gbid('removeLimitBtn').hidden = false;
    }, 100)
}


// Returns new note ID
// Checks if limit is removed (if more than 5)
function addNote() {
    if (notes.length >= 5 && isLimited()) {
        alert('Notes limit is reached. Please pay to unlock more notes.');
        return;
    }
    const title = newTitle.value;
    const body = newBody.value;
    const noteId = (new Date()).getTime()
    newTitle.value = '';
    newBody.value = '';
    notes.push({
        title,
        body,
        id: noteId
    })
    refreshNotes();
    return noteId;
}

function refreshNotes() {
    let ret = '';
    for (let i = 0; i < notes.length; i++) {
        let note = notes[i];
        ret += `<div>
        <p>${note.title}</p>
        <p>${note.body}</p>
        <button onclick="deleteNote(${note.id})">Delete note</button>
        <button onclick="editNote(${note.id})">Edit note</button>
        </div> <hr>`
    }
    gbid('notes').innerHTML = ret;
    saveNotes();
}

// Used to display note or
// to set fields when editing a note
function getNote(noteId) {
    for (let i = 0; i < notes.length; i++) {
        if (notes[i].id == noteId) return notes[i];
    }
    return null;
}

function getAllNotes() { return notes; }

// Save to local storage
function saveNotes() {
    const uid = getUid();
    saveValue(`${uid}-notes`, JSON.stringify(notes));
}

// Used as part of init
function loadNotes() {
    const uid = getUid();
    notes = JSON.parse(loadValue(`${uid}-notes`)) || []
}

function editNote(noteId) {
    let note = getNote(noteId);
    newTitle.value = note.title
    newBody.value = note.body;
    gbid('addNoteBtn').hidden = true;
    gbid('saveNoteBtn').hidden = false;
    editingNoteId = noteId;
}

function finishEditing() {
    const title = newTitle.value;
    const body = newBody.value;
    newTitle.value = '';
    newBody.value = '';
    gbid('addNoteBtn').hidden = false;
    gbid('saveNoteBtn').hidden = true;
    for (let i = 0; i < notes.length; i++) {
        if (notes[i].id == editingNoteId) {
            notes[i].title = title;
            notes[i].body = body;
        }
    }
    refreshNotes()
    editingNoteId = null;
}

function deleteNote(noteId) {
    for (let i = 0; i < notes.length; i++) {
        if (notes[i].id == noteId) {
            notes.splice(i, 1)
            refreshNotes();
            return;
        }
    }
}

// Checks balance and decrements it accordingly
// Saves info that limit was removed
// Uses payments methods
function removeLimit() {
    alert('Feature not implemented');
}

// Returns if limit is not removed
function isLimited() {
    // static response until payments are implemented
    return true;
}